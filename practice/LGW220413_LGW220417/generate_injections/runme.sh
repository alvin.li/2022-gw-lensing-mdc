echo "Now we are officially starting the 2022 GW Lensing MDC"
echo "Now generating mock data frame files"

# H1 frames
echo "H1 data frames"
gstlal_fake_frames \
	--data-source white \
	--output-path ./H1 \
	--gps-start-time 1334222848 \
	--output-frame-type H1_INJECTIONS \
	--gps-end-time 1334226944 \
	--color-psd /home/alvin.li/Projects/git_repos/gravitation-wave-lol/resources/O4_detector_sensitivities/O4_H1_psd.xml.gz \
	--output-frame-duration 16 \
	--output-frames-per-file 256 \
	--verbose \
	--channel-name=H1=INJECTIONS \
	--injections lensed_bbh_injections.xml.gz
