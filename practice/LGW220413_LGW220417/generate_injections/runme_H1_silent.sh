echo "Now we are officially starting the 2022 GW Lensing MDC"
echo "Now generating mock data frame files"

# H1 frames
echo "H1 data frames"
gstlal_fake_frames \
	--data-source silence \
	--output-path ./H1_silence/lensed \
	--gps-start-time 1333872942 \
	--output-frame-type H1_INJECTIONS \
	--gps-end-time 1333872982 \
	--output-frame-duration 16 \
	--output-frames-per-file 256 \
	--verbose \
	--channel-name=H1=INJECTIONS \
	--injections lensed_bbh_injections.xml.gz
