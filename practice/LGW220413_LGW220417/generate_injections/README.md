# Welcome to the resource page for the practice pair LGW220413-LGW220417

## Introduction
As a preliminary test, the MDC committee is providing mock data for a sample lensed pair for the different lensing pipelines to validate that their pipeline is working properly, before moving on to the actual stage 2 MDC.

LGW220413-LGW220417 is a pair of strongly galaxy-lensed events under the SIE model. One of the event, LGW220413, is further being microlensed by a point mass lens with lens parameters $`M_{L,z} = 2000`$ and $`y=0.5`$.

## Cosmology
Here we assume the O3b Power-Law-Plus-Peak model for population. We assume the Madau & Dickinson (2014) redshift evolution of the merger rate density, specified by equation 4 of the [O3 populations paper](https://arxiv.org/pdf/2111.03604.pdf).

## Parameter estimation for the two events
1. LGW220413
   - Note 1: ProdF3 and ProdF4 differ slightly in terms of the prior range for the chirp mass. ProdF4 has a slightly broader prior. 
   - Note 2: ProdF4 results are ~~preliminary~~ are now complete and ready to be used. 

   | Production run | Webpage | Run directory |
   | ------ | ------ | ------ | 
   | ProdF3 | [Link](https://ldas-jobs.ligo.caltech.edu/~alvin.li/2022_lensing_MDC/pipeline_validation/galaxy_lens/LGW220413/ProdF3/home.html) | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/PE_followup/galaxy_lens_plus_microlensing/LGW220413/ProdF3 |
   | ProdF4 | [Link](https://ldas-jobs.ligo.caltech.edu/~alvin.li/2022_lensing_MDC/pipeline_validation/galaxy_lens/LGW220413/ProdF4/home.html) | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/PE_followup/galaxy_lens_plus_microlensing/LGW220413/ProdF4 |

2. LGW220417
   - Note 1: ProdF1 posterior for chirp mass cut-off rather sharply at 60 (although the peak is still visible)
   - Note 2: ProdF3 **FINALIZED** results are now available. The chirp mass posterior is no longer railing against prior. 
   - Note 3: ProdF2 results are now available. 

   | Production run | Webpage | Run directory |
   | ------ | ------ | ------ | 
   | ProdF1 | [Link](https://ldas-jobs.ligo.caltech.edu/~alvin.li/2022_lensing_MDC/pipeline_validation/galaxy_lens/LGW220417/ProdF1/home.html) | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/PE_followup/galaxy_lens_plus_microlensing/LGW220417/ProdF1 |
   | ProdF2 | [Link](https://ldas-jobs.ligo.caltech.edu/~alvin.li/2022_lensing_MDC/pipeline_validation/galaxy_lens/LGW220417/ProdF2/home.html) | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/PE_followup/galaxy_lens_plus_microlensing/LGW220417/ProdF2 |
   | ProdF3 | [Link](https://ldas-jobs.ligo.caltech.edu/~alvin.li/2022_lensing_MDC/pipeline_validation/galaxy_lens/LGW220417/ProdF3/home.html) | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/PE_followup/galaxy_lens_plus_microlensing/LGW220417/ProdF3 |

## Source parameters (in detector frame)
- The galaxy lens parameters can be found below:

  | Image | Lens redshift $`z_L`$ | Source redshift $`z_s`$ | Magnification $`\mu`$ | Relative time delay $`\Delta t`$ (days) | Image type |
  | ------ | ------ | ------ | ------ | ------ | ------ |
  | 1 | $`0.49`$ | $`1.83`$ | $`3.364`$ | $`0.00`$ | I |
  | 2 | $`0.49`$ | $`1.83`$ | $`-1.947`$ | $`4.089`$ | II |

- You can find the source parameters in **detector frame** below:

  | Image | GPS time | Mass 1 ($`M_\odot`$) | Mass 2 ($`M_\odot`$) | Dimensionless spin 1 | Dimensionless spin 2 | Latitude | Longitude | inclination | polarization | coalescence phase | Distance (Mpc) |
  | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
  | 1 | $`1333872962.87`$ | $`93.2`$ | $`73.6`$ | ($`0.58`$, $`0.28`$, $`0.60`$) | ($`0.05`$, $`0.09`$, $`-0.21`$) | $`2.80`$ | $`1.56`$ | $`0.07`$ | $`1.97`$ | $`1.58`$ | $`7793.6`$ |
  | 2 | $`1334226252.47`$ | $`93.2`$ | $`73.6`$ | ($`0.58`$, $`0.28`$, $`0.60`$) | ($`0.05`$, $`0.09`$, $`-0.21`$) | $`2.80`$ | $`1.56`$ | $`0.07`$ | $`1.97`$ | $`1.58`$ | $`10244.3`$ |

- In the injection file (see below), the image type is specified under the `alpha3` column, in which `1`, `2`, `3` denotes Type I, Type II and Type III image respectively. Any extra amplification factor array is stored under the `numrel_data` column, which, by default, is the `default_identity_array.txt` in which the amplification factor is identically $`1`$ for all frequencies.

## Frame files
- All frame files have uniform channel names `INJECTIONS`. Because of size limit, we are not uploading the frame files to this git repository. Please copy the frame files as needed according to the information below.

- LGW220413
  1. Frame files with Gaussian noise as background

    | IFO | File path |
    | ------ | ------ |
    | H1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/H1/H-H1_INJECTIONS-13338/H-H1_INJECTIONS-1333872764-1924.gwf |
    | L1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/L1/L-L1_INJECTIONS-13338/L-L1_INJECTIONS-1333872764-1924.gwf |
    | V1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/V1/V-V1_INJECTIONS-13338/V-V1_INJECTIONS-1333872764-1924.gwf |

  2. Frame files with silent background (only for `H1`)
  
    | Type | File path |
    | ------ | ------ |
    | Lensed | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/H1_silence/lensed/H-H1_INJECTIONS-13338/H-H1_INJECTIONS-1333872942-40.gwf |
    | Unlensed | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/H1_silence/unlensed/H-H1_INJECTIONS-13338/H-H1_INJECTIONS-1333872942-40.gwf |

- LGW220417
  1. Frame files with Gaussian noise as background

    | IFO | File path |
    | ------ | ------ |
    | H1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/H1/H-H1_INJECTIONS-13342/H-H1_INJECTIONS-1334222850-4092.gwf |
    | L1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/L1/L-L1_INJECTIONS-13342/L-L1_INJECTIONS-1334222850-4092.gwf |
    | V1 | CIT:/home/alvin.li/Projects/2022_lensing_MDC/pipeline_validation/generate_injections/V1/V-V1_INJECTIONS-13342/V-V1_INJECTIONS-1334222850-4092.gwf |

## Injection file
You can find the injection file [here](lensed_bbh_injections.xml.gz).

## Detailed answer key
The detailed answer key can be found [here](answer_key.csv).

## Example python notebooks
1. Plotting the lensed and unlensed waveform (LGW220413)
   - An example notebook to plot the lensed and unlensed waveforms for LGW220413 can be found [here](code_review.ipynb). It also includes a sanity check for LALSimulation using PyCBC.

2. Plotting the Qscans from frame files
   - An example notebook to get the Qscan from a frame file can be found [here](Qscan.ipynb). For LENSID, one can make use of this notebook to get the Qscans for the lensing analysis. (**DISCLAIMER**: The Qscan settings have not been optimized.)

## Microlensing amplification factor array
The amplification factor array associated with the microlens for LGW220413 can be found [here](point_mlz_2000_y_0.5.dat).

## Original lensed sample set
You can find information of the original lensed sample set (set 161) [here](super_super_set_161.csv). Note that the source parameters are measured in the detector frame, except for the source and lens `z_lens` and `z_source`.

## Bayestar Skymaps
- We did not run a matched-filtering pipeline run for the mock data. To generate Bayestar skymap, we use `bayestar-realize-coinc` to simulate skymaps for the injections. [**DISCLAIMER**: The skymaps are slightly better than expected because we cannot modify the waveform with the extra microlensing effect, i.e. The skymaps generated are only for the **strongly lensed** images without microlensing.]

- You can find the skymaps for LGW220413 [here](bayestar_simulation/LGW220413.fits) and for LGW220417 [here](bayestar_simulation/LGW220417.fits).

- You can view the visualizations of the two skymaps [here](bayestar_simulation/LGW220413_LGW220414_skymaps.pdf). Special thanks for Rico's [skymap overlap code](https://git.ligo.org/ka-lok.lo/skymap-overlap/-/tree/master).
