#V1_frames
echo "V1 data frames"
gstlal_fake_frames \
	--data-source white \
	--output-path ./V1 \
	--gps-start-time 1334222848 \
	--output-frame-type V1_INJECTIONS \
	--gps-end-time 1334226944 \
	--color-psd /home/alvin.li/Projects/git_repos/gravitation-wave-lol/resources/O4_detector_sensitivities/O4_V1_psd.xml.gz \
	--output-frame-duration 16 \
	--output-frames-per-file 256 \
	--verbose \
	--channel-name=V1=INJECTIONS \
	--injections lensed_bbh_injections.xml.gz


