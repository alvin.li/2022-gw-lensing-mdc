# Generate H1 psd
gstlal_psd_xml_from_asd_txt --instrument H1 --output O4_H1_psd.xml.gz aligo_O4high.txt 

# Generate L1 psd
gstlal_psd_xml_from_asd_txt --instrument L1 --output O4_L1_psd.xml.gz aligo_O4high.txt 

# Generate V1 psd
gstlal_psd_xml_from_asd_txt --instrument V1 --output O4_V1_psd.xml.gz avirgo_O4high_NEW.txt
