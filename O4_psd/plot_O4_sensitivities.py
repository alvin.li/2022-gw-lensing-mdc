#!/usr/bin/env python3

# Copyright (C) 2022  Alvin Ka Yue Li, Juno Chun Lung Chan
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import numpy
import pandas
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt

# Load in the O4 sensitivities
H1L1_asd = pandas.read_csv("aligo_O4high.txt", names = ['f', 'amplitude'], delimiter = " ")
V1_asd = pandas.read_csv("avirgo_O4high_NEW.txt", names = ['f', 'amplitude'], delimiter = " ")

# Plot the sensitivities (ASDs)
plt.loglog(H1L1_asd.f, H1L1_asd.amplitude, label = "aLIGO O4 (Expected)")
plt.loglog(V1_asd.f, V1_asd.amplitude, label = "aVIRGO O4 (Expected)")
plt.xlabel(r"Frequency $f$ (Hz)")
plt.ylabel(r"Strain $(1/\sqrt{\mathrm{Hz}})$")
plt.xlim(min(H1L1_asd.f.min(), V1_asd.f.min()), max(H1L1_asd.f.max(), V1_asd.f.max()))
plt.grid()
plt.legend()
plt.savefig("O4_expected_noise_curves.pdf")
