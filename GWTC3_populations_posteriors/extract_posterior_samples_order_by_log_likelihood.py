#!/usr/bin/env python3

import pandas
import json

f = open("o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_result.json")
data = json.load(f)

posteriors = pandas.DataFrame(data['posterior']['content'])
posteriors = posteriors.sort_values(by='log_likelihood', ascending=False)

posteriors.to_csv("PLPP_model_posteriors.csv", index=False)
