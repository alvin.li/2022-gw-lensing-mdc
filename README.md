# 2022-GW-Lensing-MDC
This git repository serves as a centralized resource hub for analysts to get official resources for analysis.

# GraceDB-playground
All found super-threshold gravitational waves will be uploaded to GraceDB playground under the label `2022_LENSING_MDC`. You can click on [this link](https://gracedb-playground.ligo.org/latest/?query=2022_LENSING_MDC&query_type=E&get_neighbors=&results_format=) to get the latest search results.
